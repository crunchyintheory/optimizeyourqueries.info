const input: HTMLTextAreaElement = document.querySelector('#input');
const output: HTMLElement = document.querySelector('#output');

input.addEventListener('input', () => {
    output.innerText = 
        input.value.toLowerCase()
            .replace(/('|"|`) = ('|"|`)/g, '$1=$2');
});