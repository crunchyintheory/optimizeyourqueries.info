const gulp = require('gulp');
const sass = require('gulp-sass');
const ts = require('gulp-typescript');

gulp.task('sass', function() {
    return gulp.src('./style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public'));
});

gulp.task('ts', function() {
    return gulp.src('./script.ts')
        .pipe(ts({
            outFile: 'script.js',
            noImplicitAny: true
        }))
        .pipe(gulp.dest('public'));
})

gulp.task('move', function() {
    return gulp.src([
        './index.html',
        './icon.png'
    ], { base: './' })
        .pipe(gulp.dest('public'));
});

gulp.task('default', ['move', 'ts', 'sass']);
